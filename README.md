# Safeleads Test

### Instalação e Execução

Para instalar o projeto, o destinatário deverá copiar e colar o seguinte código no terminal do diretório que deseja trabalhar:

```bash
git clone https://gitlab.com/GutoFrr/safeleads-test.git .
```

Baixar as dependências:

```powershell
npm install
```

Comando para executar como Single Page Application (SPA):

```powershell
npm run dev
```

Comando para executar como Progressive Web Application (PWA):

```powershell
npm run dev-pwa
```
