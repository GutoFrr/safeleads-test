import { defineStore } from 'pinia';
import { LocalStorage } from 'quasar';

type Todo = {
  content: string;
  completed: boolean;
  editing: boolean;
};

type TodoStore = {
  todo: Todo;
  todos: Todo[];
  editing: boolean;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type TodoGetters = any;

type TodoActions = {
  addTodo(): void;
  toggleTodoCompletion(id: number): void;
  setEditTodo(id: number): void;
  editTodo(): void;
  deleteTodo(id: number): void;
};

export const useTodoStore = defineStore<
  string,
  TodoStore,
  TodoGetters,
  TodoActions
>('todo', {
  state: () => ({
    todo: {
      content: '',
      completed: false,
      editing: false,
    },
    todos: LocalStorage.isEmpty()
      ? []
      : JSON.parse(LocalStorage.getItem('todos') as string),
    editing: false,
  }),
  getters: {},
  actions: {
    addTodo() {
      this.todos.push({ ...this.todo, content: this.todo.content });
      this.todo = { completed: false, content: '', editing: false };
      LocalStorage.set('todos', JSON.stringify(this.todos));
    },
    toggleTodoCompletion(id: number) {
      this.todos = this.todos.map((item, index) => {
        if (index === id) {
          return { ...item, completed: !item.completed };
        } else {
          return item;
        }
      });
      LocalStorage.set('todos', JSON.stringify(this.todos));
    },
    setEditTodo(id: number) {
      this.editing = true;
      this.todos = this.todos.map((item, index) => {
        if (id === index) {
          this.todo = item;
          return { ...item, editing: true };
        } else {
          return item;
        }
      });
    },
    editTodo() {
      this.todos = this.todos.map((item) => {
        if (item.editing) {
          return { ...item, content: this.todo.content, editing: false };
        } else {
          return item;
        }
      });
      this.todo = { completed: false, content: '', editing: false };
      this.editing = false;
      LocalStorage.set('todos', JSON.stringify(this.todos));
    },
    deleteTodo(id: number) {
      this.todos = this.todos.filter((_, index) => index !== id);
      LocalStorage.set('todos', JSON.stringify(this.todos));
    },
  },
});
